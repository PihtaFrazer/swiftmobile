//
//  RightPanel.swift
//  EOSSwiftMobile
//
//  Created by MininA on 03/02/2020.
//  Copyright © 2020 MininA. All rights reserved.
//

import UIKit

class RightPanel: UIViewController {

    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameOfEmployee: UILabel!
    @IBOutlet weak var nameOfPosition: UILabel!
    @IBOutlet weak var empty: UILabel!
    @IBOutlet weak var tableViewController: UITableView!
	
    var numberOfRows = 2
    var arrayInt: [Int] = [1,2]
	var statement: Bool = false
	var section = 1
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        tableViewController.delegate = self
        tableViewController.dataSource = self
	
        coloringButton()
    }
    
    func coloringButton() {
        
//        sendToExecution.layer.cornerRadius = 15
//        sendToExecution.setTitle("Отправить поручение", for: .normal)
//        sendToExecution.layer.backgroundColor = UIColor.blue.cgColor
//
//        addProjectOutlet.layer.cornerRadius = 15
//        addProjectOutlet.layer.backgroundColor = UIColor.blue.cgColor
    }
  
	
// удаляем пректы поручений
    @objc func closeProject(_ sender: UIButton) {
        if let cell = sender.superview?.superview as? RightPanelTableVIewCells, let indexPath = tableViewController.indexPath(for: cell) {
            arrayInt.remove(at: indexPath.row)
            numberOfRows -= 1
			section -= 1
			tableViewController.deleteSections([indexPath.section], with: .left)
         //   tableViewController.deleteRows(at: [indexPath], with: .left)
        }
    }
	
	
	
	@objc func addEmployee(_ sender: UIButton) {
		if let cell = sender.superview?.superview as? RightPanelTableVIewCells, let indexPath = tableViewController.indexPath(for: cell) {
		        numberOfRows += 1
			//print("print indexpath = \(indexPath.row)")
			arrayInt.insert(numberOfRows, at: indexPath.row + 1)
		        let indexPath = IndexPath(row: arrayInt.count - 1, section: 0)
		        tableViewController.insertRows(at: [indexPath], with: .automatic)
		
		        switch numberOfRows {
		        case 0:
		            empty.isHidden = false
		        default:
					empty.isHidden = true
		        }
		
		        tableViewController.reloadRows(at: [indexPath], with: .left)
		}
	}
	

	
// действие для добавления поручения
//    @IBAction func addProject(_ sender: UIButton) {
//        numberOfRows += 1
//        arrayInt.append(numberOfRows)
//        let indexPath = IndexPath(row: arrayInt.count - 1, section: 0)
//        tableViewController.insertRows(at: [indexPath], with: .automatic)
//
//        switch numberOfRows {
//        case 0:
//            empty.isHidden = false
//        default:
//			empty.isHidden = true
//        }
//
//        tableViewController.reloadRows(at: [indexPath], with: .left)
//    }
    
	// тестовый вариант для отчетов
//	@objc @IBAction func addActionDropDownMenu(_ sender: UIButton) {
//
//		if let cell = sender.superview?.superview as? RightPanelTableVIewCells, let indexPath = tableViewController.indexPath(for: cell) {
//			switch statement {
//			case true:
//				cell.constraintHight.constant = 0
//				tableViewController.reloadRows(at: [indexPath], with: .automatic)
//				statement = false
//			case false:
//				cell.constraintHight.constant = 128
//				tableViewController.reloadRows(at: [indexPath], with: .automatic)
//				statement = true
//			}
//
//		}
//	}
	
	// Добавление текстового поля
//	@IBAction func addEmployeeTF(_ sender: UIButton) {
//		if let cell = sender.superview?.superview as? RightPanelTableVIewCells {
//			switch sender.tag {
//			case 0:
//				cell.TfEmployeeCollection[1].isHidden = !cell.TfEmployeeCollection[1].isHidden
//				cell.addEmployeeTF[1].isHidden = !cell.addEmployeeTF[1].isHidden
//			case 1:
//				cell.TfEmployeeCollection[2].isHidden = !cell.TfEmployeeCollection[2].isHidden
//				cell.addEmployeeTF[2].isHidden = !cell.addEmployeeTF[2].isHidden
//			case 2:
//				cell.TfEmployeeCollection[3].isHidden = !cell.TfEmployeeCollection[3].isHidden
//				cell.addEmployeeTF[3].isHidden = !cell.addEmployeeTF[3].isHidden
//			case 3:
//				cell.TfEmployeeCollection[4].isHidden = !cell.TfEmployeeCollection[4].isHidden
//				cell.addEmployeeTF[4].isHidden = !cell.addEmployeeTF[4].isHidden
//			case 4:
//			cell.TfEmployeeCollection[5].isHidden = !cell.TfEmployeeCollection[5].isHidden
//				cell.addEmployeeTF[5].isHidden = !cell.addEmployeeTF[5].isHidden
//			default:
//				print("err")
//			}
//
//		}
//	}
}

extension RightPanel: UITableViewDataSource, UITableViewDelegate {
    
	func numberOfSections(in tableView: UITableView) -> Int {
		return section
	}
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return arrayInt.count
		}
		return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
//		if indexPath.row == 0 {
//		if let cell = tableView.dequeueReusableCell(withIdentifier: "projectCells",
//													for: indexPath) as? RightPanelTableVIewCells {
//
//			cell.delRowOutlet.tag = indexPath.row
//			//		cell.dropDownMenu.addTarget(self,
//			//									action: #selector(addActionDropDownMenu(_:)),
//			//									for: .touchUpInside)
//			        cell.delRowOutlet.addTarget(self,
//			                                    action: #selector(closeProject(_:)),
//			                                    for: .touchUpInside)
//			cell.projectNumber.text = "№ \(indexPath.section + 1)"
//			print(arrayInt)
//			return cell
//			}
//
//		}
//		if indexPath.row == 1 {
//			if let cell = tableView.dequeueReusableCell(withIdentifier: "cellTF", for: indexPath) as? RightPanelTableVIewCells {
//			print("sdsd")
//			cell.addEmployee.addTarget(self, action: #selector(addEmployee(_:)), for: .touchUpInside)
//			cell.layer.backgroundColor = UIColor.blue.cgColor
//			return cell
//			}
//		}
//        return UITableViewCell()
		
		
		if indexPath.row == 0 {
		if let cell = tableView.dequeueReusableCell(withIdentifier: "projectCells",
													for: indexPath) as? RightPanelTableVIewCells {

			cell.delRowOutlet.tag = indexPath.row
//					cell.dropDownMenu.addTarget(self,
//												action: #selector(addActionDropDownMenu(_:)),
//												for: .touchUpInside)
			        cell.delRowOutlet.addTarget(self,
			                                    action: #selector(closeProject(_:)),
			                                    for: .touchUpInside)
			cell.projectNumber.text = "№ \(indexPath.section + 1)"
			print(arrayInt)
			return cell
			}
			
		}
		if indexPath.row >= 1 {
			if let cell = tableView.dequeueReusableCell(withIdentifier: "cellTF", for: indexPath) as? RightPanelTableVIewCells {
			print("sdsd")
			cell.addEmployee.addTarget(self, action: #selector(addEmployee(_:)), for: .touchUpInside)
			return cell
			}
		}
        return UITableViewCell()
    }
	}

