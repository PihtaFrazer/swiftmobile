//
//  RightPanelTableVIewCells.swift
//  EOSSwiftMobile
//
//  Created by MininA on 03/02/2020.
//  Copyright © 2020 MininA. All rights reserved.
//

import UIKit

class RightPanelTableVIewCells: UITableViewCell {
    
    
    override func prepareForReuse() {
           super.prepareForReuse()
           self.accessoryType = .none
    }

    @IBOutlet weak var addEmployee: UIButton!
    
    @IBOutlet weak var delRowOutlet: UIButton!
    @IBOutlet weak var projectNumber: UILabel!    
}
